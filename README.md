# Multilanguage name translator

**Popis cíle**

Mým cílem bylo vytvořit a natrénovat model, který
jako vstup dostane latinkou psané jméno, a na výstup vytvoří transkripci v azbuce.

Je to "kreativní" úprava zadání, které původně bylo vytvořit model, který
porovnává jména psaná jinými jazyky/písmy. Místo toho tedy vytvářím překladač,
který jméno na vstupu přepíše z latinky do azbuky.

**Vstupní data**

Vstupním trénovacím souborem je https://github.com/steveash/NETransliteration-COLING2018/blob/master/data/wd_russian.

Notebook (v goolgle colab) předpokládá umístění tohoto souboru v "drive/My Drive/wd_russian.txt"

Celý projekt je v notebooku translator.ipynb zde v kořenu adresáře, ovšem z nějakého důvodu se mi nedá otevřít přímo zde v GitLabu (stránka ze zasekne).

# Milestone 1.12.

**Název práce:**

Multilingual names
    
**Popis práce:**

Pokouším se natrénovat model, aby z anglického jména nebo názvu (či možná i jinojazyčného jména psaného latinkou)
vytvořil ruskou transkripci v azbuce.

**Co z toho leze v současném stavu:**

Zatím tam mám velmi primitivní model (jen DNN), výsledkem je, zdá se, jednoduchý
transkriptor, který přepisuje znak po znaku nehledě na okolní kontext.

Například na "Unexpected mutation badger" odpoví "Унекпектед мутатион бадгер"

**Co je v plánu:**

Vylepšit model tak, aby se díval na okolní kontext znaků, mám zatím v plánu zkusit zapojit konvoluční vrstvy.

